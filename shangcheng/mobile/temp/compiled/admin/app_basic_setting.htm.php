<?php echo $this->fetch('pageheader.htm'); ?>
<?php echo $this->smarty_insert_scripts(array('files'=>'../js/utils.js')); ?>
<form enctype="multipart/form-data" action="app_manage.php" method="post">
<table width="90%">
<?php $_from = $this->_var['basic_setting']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'var');if (count($_from)):
    foreach ($_from AS $this->_var['var']):
?>
<tr>
<td class="label" valign="top"><?php echo $this->_var['var']['name']; ?></td>
<td>
<?php if ($this->_var['var']['type'] == "text"): ?>
<input type="text" name="value[<?php echo $this->_var['var']['code']; ?>]" value="<?php echo $this->_var['var']['value']; ?>" />
<?php elseif ($this->_var['var']['type'] == "select"): ?>
<?php $_from = $this->_var['var']['store_options']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('k', 'opt');if (count($_from)):
    foreach ($_from AS $this->_var['k'] => $this->_var['opt']):
?>
<label for="value_<?php echo $this->_var['var']['id']; ?>_<?php echo $this->_var['k']; ?>">
	<input type="radio" name="value[<?php echo $this->_var['var']['code']; ?>]" id="value_<?php echo $this->_var['var']['id']; ?>_<?php echo $this->_var['k']; ?>" value="<?php echo $this->_var['opt']; ?>"
<?php if ($this->_var['var']['value'] == $this->_var['opt']): ?>checked="true"<?php endif; ?>/><?php echo $this->_var['var']['display_options'][$this->_var['k']]; ?></label>
<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
<?php endif; ?>
</td>
</tr>
<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
</table>
<div class="button-div">
<input type="submit" value="确定" class="button" />
<input type="reset" value="重置" class="button" />
<input type="hidden" name="act" value="save_basic_setting" />
</div>
</form>

<script language="JavaScript">
onload = function()
{
    // 开始检查订单
    startCheckOrder();
}
</script>

<?php echo $this->fetch('pagefooter.htm'); ?>