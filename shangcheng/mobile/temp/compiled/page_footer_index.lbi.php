<div class="site-footer">
	<div class="footer-service">
        <div class="w1210">
          <ul class="list-service clearfix">
            <li><a class="ic1" rel="nofollow" href="" target="_blank"><i></i><strong>1小时快修服务</strong></a> </li>
            <li><a class="ic2" rel="nofollow" href="" target="_blank"><i></i><strong>7天无理由退货</strong></a> </li>
            <li><a class="ic3" rel="nofollow" href="" target="_blank"><i></i><strong>15天免费换货</strong></a> </li>
            <li><a class="ic4" rel="nofollow" href="" target="_blank"><i></i><strong>满150元包邮</strong></a> </li>
            <li><a class="ic5" rel="nofollow" href="" target="_blank"><i></i><strong>460余家售后网点</strong></a> </li>
          </ul>
        </div>
    </div>
    <div class="footer-related">
        <div class="footer-article w1210"> 
          <?php $_from = $this->_var['helps']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'help_cat');$this->_foreach['no'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['no']['total'] > 0):
    foreach ($_from AS $this->_var['help_cat']):
        $this->_foreach['no']['iteration']++;
?>
          <dl class="col-article <?php if (($this->_foreach['no']['iteration'] <= 1)): ?>col-article-first<?php endif; ?>">
            <dt><?php echo $this->_var['help_cat']['cat_name']; ?></dt>
            <?php $_from = $this->_var['help_cat']['article']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'item_0_13500000_1614826016');if (count($_from)):
    foreach ($_from AS $this->_var['item_0_13500000_1614826016']):
?>
            <dd><a rel="nofollow" href="help.php?id=<?php echo $this->_var['item_0_13500000_1614826016']['article_id']; ?>" target="_blank"><?php echo $this->_var['item_0_13500000_1614826016']['short_title']; ?></a></dd>
            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
          </dl>
          <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
          <dl class="col-article">
                <dt>服务支持</dt>
                <dd>售前咨询 400-000-0000</dd>
                <dd>售后咨询 400-000-0000</dd>
                <dd>客服 QQ 570830288</dd>
                <dd>工作时间 9：00-21：00</dd>
           </dl>
        </div>
    	<div class="footer-info clearfix" >
        <div class="info-text">
        <?php if ($this->_var['img_links'] || $this->_var['txt_links']): ?> 
        <p>友情链接：
            <?php $_from = $this->_var['img_links']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'link');if (count($_from)):
    foreach ($_from AS $this->_var['link']):
?>
            <a href="<?php echo $this->_var['link']['url']; ?>" target="_blank" title="<?php echo $this->_var['link']['name']; ?>"><?php echo $this->_var['link']['name']; ?></a><span class="sep">|</span>
            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
            <?php $_from = $this->_var['txt_links']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'link');$this->_foreach['name'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['name']['total'] > 0):
    foreach ($_from AS $this->_var['link']):
        $this->_foreach['name']['iteration']++;
?>
            <a href="<?php echo $this->_var['link']['url']; ?>" target="_blank" title="<?php echo $this->_var['link']['name']; ?>"><?php echo $this->_var['link']['name']; ?></a><?php if (! ($this->_foreach['name']['iteration'] == $this->_foreach['name']['total'])): ?><span class="sep">|</span><?php endif; ?>
            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
        </p>
        <?php endif; ?>
        <p class="nav_bottom">
            <?php if ($this->_var['navigator_list']['bottom']): ?>
            <?php $_from = $this->_var['navigator_list']['bottom']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'nav_0_13500000_1614826016');$this->_foreach['nav_bottom_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['nav_bottom_list']['total'] > 0):
    foreach ($_from AS $this->_var['nav_0_13500000_1614826016']):
        $this->_foreach['nav_bottom_list']['iteration']++;
?>
            <a href="<?php echo $this->_var['nav_0_13500000_1614826016']['url']; ?>"  <?php if ($this->_var['nav_0_13500000_1614826016']['opennew'] == 1): ?>target="_blank"<?php endif; ?>><?php echo $this->_var['nav_0_13500000_1614826016']['name']; ?></a><em <?php if (($this->_foreach['nav_bottom_list']['iteration'] == $this->_foreach['nav_bottom_list']['total'])): ?>style="display:none"<?php endif; ?>>|</em>
            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
            <?php endif; ?>
            <?php if ($this->_var['icp_number']): ?>
            <?php echo $this->_var['lang']['icp_number']; ?>:<a href="http://www.miibeian.gov.cn/" target="_blank"><?php echo $this->_var['icp_number']; ?></a>
            <?php endif; ?>
        </p>
        <p>
            <a href="javascript:;"><?php echo $this->_var['copyright']; ?></a> <a href="javascript:;"><?php echo $this->_var['shop_address']; ?> <?php echo $this->_var['shop_postcode']; ?></a>
            <a href="javascript:;"><?php if ($this->_var['service_phone']): ?>
            Tel: <?php echo $this->_var['service_phone']; ?>
            <?php endif; ?></a>
            <a href="javascript:;"><?php if ($this->_var['service_email']): ?>
            E-mail: <?php echo $this->_var['service_email']; ?>
            <?php endif; ?></a>
        </p>
        <p>
            <a href="/" target="_blank" title="诚信示范单位"><img src="/themes/68ecshopcom_360buy/images/footer_1.png" alt="诚信示范单位" /></a>
            <a href="/" target="_blank" title="官网认证"><img src="/themes/68ecshopcom_360buy/images/footer_2.png" alt="官网认证" /></a>
            <a href="/" target="_blank" title="可信网站"><img src="/themes/68ecshopcom_360buy/images/footer_3.png" alt="可信网站" /></a>
            <a href="/" target="_blank" title="实名验证"><img src="/themes/68ecshopcom_360buy/images/footer_4.png" alt="实名验证" /></a>
            <a href="/" target="_blank" title="认证联盟行业网站"><img src="/themes/68ecshopcom_360buy/images/footer_5.png" alt="认证联盟行业网站" /></a>
            <a href="/" target="_blank" title="360网站安全检测"><img src="/themes/68ecshopcom_360buy/images/footer_6.png" alt="360网站安全检测" /></a>
		</p>
		
      </div>      
    </div>    
  </div>
</div>

<img src="api/cron.php?t=" alt="" style="width:0px;height:0px;" />


<script type="text/javascript">
Ajax.call('api/okgoods.php', '', '', 'GET', 'JSON');
$("img").lazyload({
    effect       : "fadeIn",
	 skip_invisible : true,
	 failure_limit : 20
});
</script>

<script type="text/javascript" src="themes/68ecshopcom_360buy/js/base.js" ></script>